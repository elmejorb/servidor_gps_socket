var express = require("express");
var app = express();
var server_exp = require("http").Server(app);
var io = require("socket.io")(server_exp);
var https = require('https');
var latitud = 0;
var longitud = 0;
var imei = 0;
var velocidad = 0;
var datospuntos = [];
const markers = [];

const dgram = require('dgram');

const server = dgram.createSocket('udp4');

const HOST = '0.0.0.0';
const PORT = 8090;

server.on('error', (err) => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});

app.use(express.static("public"));

app.get('/mapa', (req, res) => {
    
    res.status(200).send("OK");
    res.sendFile(__dirname + '/mapa.html');
  });

server.on('message', (msg, rinfo) => {

    let bufff = Buffer.from(msg);
    let data = bufff.toString();
    
    

    var indentifi = data.substr(0,5);

    
    if (indentifi == "*GS06" && data.length > 60){
        var partes_data = data.split(",");
        var coordenadas = partes_data[5];
        
        var part_coordenadas = coordenadas.split(";");
        carac_lon = part_coordenadas[3].substr(0,1);
        if (carac_lon == "W")
            carac_lon = "-";

        latitud = part_coordenadas[2].substr(1, part_coordenadas[2].length);
        longitud = carac_lon + part_coordenadas[3].substr(1, part_coordenadas[3].length);
        velocidad = part_coordenadas[4];
        imei = partes_data[1];
/*
        console.log("-------------------------------------------------");
        console.log(`IP: ${rinfo.address}:${rinfo.port}`);
        console.log("IMEI:" + imei);
        console.log("FECHA-HORA:" + partes_data[2]);
        console.log("LATITUD:" + latitud);
        console.log("LONGITUD:" + longitud);
  */      
        puntos =  {"imei":imei, "lat":latitud, "long":longitud, "veloc":velocidad};
         
        for (var i = 0; i < datospuntos.length; i++){
            var obj = datospuntos[i];
            console.log(obj["imei"], imei);
            if (obj["imei"] == imei){
                datospuntos.splice(i,1); 
            }
        }
        datospuntos.push(puntos); 

        /*datospuntos.forEach(p => {
            if (p.imei == imei){
                datospuntos.push(puntos);              
            }
        }); */
       
        
    }else{
        console.log("-----------------TRAMA NO VALIDA ID: " + indentifi + " --------------------");
    }

        
    //console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
  });

  function saveCoordenadas(){
    const data = JSON.stringify({
      imei: imei,
      lat: latitud,
      long: longitud,
      veloc: velocidad
    });

    https.get('https://conelecap.com.co/canalesycontactos/pages/saveposition/?dato='+data, function(res){
      var str = '';
      console.log('Response is '+res.statusCode);

      res.on('data', function (chunk) {
             str += chunk;
       });

      res.on('end', function () {
           console.log(str);
      });

    });

/*
    const data = JSON.stringify({
      dato: 'Buy the milk'
    })
    
    const options = {
      hostname: '10.147.20.190/luis/pages/saveposition/',
      port: 80,
      path: '/',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': data.length
      }
    }
    
    const req = http.request(options, res => {
      console.log(`statusCode: ${res.statusCode}`)
    
      res.on('data', d => {
        process.stdout.write(d)
      })
    })
    
    req.on('error', error => {
      console.error(error)
    })
    
    req.write(data)
    req.end()*/
}

server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
    /*
    var data = "*GS06,352556100606404,200355250821,,SYS:SP4603NS;V3.40;V1.1.5,GPS:V;0;N0.000000;E0.000000;0;0;0;0.00,COT:0,ADC:10.67;3.83,DTT:4000;E0;0;0;0;1$200400250821,,SYS:SP4603NS;V3.40;V1.1.5,GPS:V;0;N0.000000;E0.000000;0;0;0;0.00,COT:0,ADC:10.82;3.84,DTT:4000;E0;0;0;0;1$200405250821,,SYS:SP4603NS;V3.40;V1.1.5,GPS:V;0;N0.000000;E0.000000;0;0;0;0.00,COT:0,ADC:10.75;3.83,DTT:4000;E0;0;0;0;1$200410250821,,SYS:SP4603NS;V3.40;V1.1.5,GPS:V;0;N0.000000;E0.000000;0;0;0;0.00,COT:0,ADC:10.81;3.85,DTT:4000;E0;0;0;0;1$200415250821,,SYS:SP4603NS;V3.40;V1.1.5,GPS:V;0;N0.000000;E0.000000;0;0;0;0.00,COT:0,ADC:10.81;3.85,DTT:4004;E0;0;0;0;1$200420250821,,SYS:SP4603NS;V3.40;V1.1.5,GPS:V;0;N0.000000;E0.000000;0;0;0;0.00,COT:0,ADC:10.68;3.83,DTT:4004;E0;0;0;0;1$200425250821,,SYS:SP4603NS;V3.40;V1.1.5,GPS:V;0;N0.000000;E0.000000;0;0;0;0.00,COT:0,ADC:10.76;3.87,DTT:4004;E0;0;0;0;1# from 191.106.180.121:7168";

    */
  });

server.bind(8090);

io.on("connection", function (socket) {
    
    var num_usuarios = 0;
    var activos = 0;
    console.log(num_usuarios, activos);
    console.log("Un cliente se ha conectado, No. " + io.engine.clientsCount);
  //  socket.emit("messages", messages);
        socket.on('chat message', (msg) => {
            console.log('message: ' + msg + " posicion: " + longitud + " - " + latitud );
            io.emit('chat message',  + msg + " posicion: " + longitud + " - " + latitud );
        });
        socket.on('disconnect', () => {
            console.log('user disconnected');
        });
 
        setInterval(() => {

          num_usuarios = io.engine.clientsCount;
          activos = socket.client.conn.server.clientsCount;

         // console.log(datospuntos);
          var envio = JSON.stringify(datospuntos);
          socket.emit('marker', envio);
          socket.emit('contador', activos);
          saveCoordenadas();
        }, 2000);
  });

  server_exp.listen(8080, function () {
    console.log("Servidor corriendo en http://localhost:8080");
    saveCoordenadas();
  });

  /*
  socket.on("new-message", function (data) {
    messages.push(data);
  
    io.sockets.emit("messages", messages);
  });*/